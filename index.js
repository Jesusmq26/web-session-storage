function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var spanValor = document.getElementById("spanValor");
  var clave = txtClave.value;
  var valor = txtValor.value;
  if(clave===""){
	  spanValor.innerText = "Ingrese la clave.";
	  document.getElementById("txtClave").focus();
  }else if(valor===""){
	  spanValor.innerText = "Ingrese el valor.";
	  document.getElementById("txtValor").focus();
  }else{
	  sessionStorage.setItem(clave, valor);
	  var objeto = {
		nombre:"Jesus",
		apellidos:"Marroquin Quijada",
		ciudad:"Lima",
		pais:"Perú"
	  };
	  sessionStorage.setItem("json", JSON.stringify(objeto));
	  spanValor.innerText = "Clave y valor registrados.";
	  txtClave.value="";
	  txtValor.value="";
  }
}
function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var spanValor = document.getElementById("spanValor");
  var clave = txtClave.value;
  if(clave===""){
	  spanValor.innerText = "Ingrese la clave";
	  document.getElementById("txtClave").focus();
  }else{
	  var valor = sessionStorage.getItem(clave);	  
	  if(valor===null){
		  spanValor.innerText = "No se encontro clave.";
	  }else{
			spanValor.innerText = "Valor: "+valor;
	  }
	  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
	  if(datosUsuario!==null){
		  console.log(datosUsuario.nombre);
		  console.log(datosUsuario.pais);
		  console.log(datosUsuario);
	  }
  }
}

function eliminarEnSessionStorage(){
	var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
	var span_valor = document.getElementById("spanValor");
	var clave = txtClave.value;
	if(clave===""){
	  spanValor.innerText = "Ingrese la clave";
	  document.getElementById("txtClave").focus();
	}else{
		var valor = sessionStorage.getItem(clave);	
		 if(valor===null){
		  spanValor.innerText = "No se encontro clave.";
		}else{
			sessionStorage.removeItem(clave);
			span_valor.innerText = "Session eliminada: "+clave;
		}
	}
}

function limpiarEnSessionStorage(){
	sessionStorage.clear();
	var spanValor = document.getElementById("spanValor");
	spanValor.innerText = "Se limpiaron todos los datos del SessionStorage.";
}

function cantidadEnSessionStorage(){
	var spanValor = document.getElementById("spanValor");
	var cantidad = sessionStorage.length;
	spanValor.innerText = "Cantidad de elementos en el SessionStorage son: "+cantidad;
}